//
//  GameViewController.swift
//  Dont let Kanye into his zone
//
//  Created by Amanda Kobiolka on 3/25/17.
//  Copyright (c) 2017 Amanda Kobiolka. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    



    override func viewDidLoad() {
            super.viewDidLoad()
            self.view?.backgroundColor = UIColor.black

            if let scene = StartScene(fileNamed:"StartScene") {
                /* Setup your scene here */

                // Configure the view.
                let skView = self.view as! SKView
                skView.showsFPS = true
                skView.showsNodeCount = true
                
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .aspectFill
                
                skView.presentScene(scene)
            }
    }

    override var shouldAutorotate : Bool {
        return true
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
}
