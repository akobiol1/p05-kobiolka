//
//  GameScene.swift
//  Dont let Kanye into his zone
//
//  Created by Amanda Kobiolka on 3/25/17.
//  Copyright (c) 2017 Amanda Kobiolka. All rights reserved.
//  Helpful tutorial: https://www.youtube.com/watch?v=I3f3uLlYc9Y

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
 
    let screenSize = UIScreen.main.bounds
    let kanye = SKSpriteNode(imageNamed: "kanye.png")
    let zone1 = SKSpriteNode(imageNamed: "zone.png")
    let ball = SKSpriteNode(imageNamed: "orange ball.png")
    //let background = SKSpriteNode(imageNamed: "blackstars.jpg")
    let kanyeCat  : UInt32 = 0x1 << 1
    let zoneCat: UInt32 = 0x1 << 2
    let ballCat : UInt32 = 0x1 << 3
    var check = false
    let sceneBody = SKPhysicsBody(edgeLoopFrom: UIScreen.main.bounds)

    override func didMove(to view: SKView) {
        //self.view?.backgroundColor = UIColor.black
        self.physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -6.0)
        /*background.anchorPoint = CGPoint.zero
        background.zPosition = 1
        addChild(background)*/
        sceneBody.friction = 0
        self.physicsBody = sceneBody
        
        kanye.size = CGSize(width: 60, height: 70)
        kanye.physicsBody = SKPhysicsBody(rectangleOf: kanye.size)
        kanye.zPosition = 2
        kanye.physicsBody?.isDynamic = true;
        kanye.position = CGPoint(x: 500, y: 800)
        kanye.physicsBody?.affectedByGravity = true
        kanye.physicsBody?.restitution = 1
        kanye.physicsBody?.categoryBitMask = kanyeCat
        kanye.physicsBody?.collisionBitMask = zoneCat | ballCat
        kanye.physicsBody?.contactTestBitMask = zoneCat | ballCat
        addChild(kanye)
        
        zone1.size = CGSize(width: 150, height: 150)
        zone1.physicsBody = SKPhysicsBody(circleOfRadius: 75.0)
        zone1.zPosition = 2
        zone1.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        zone1.physicsBody?.isDynamic = false;
        zone1.physicsBody?.affectedByGravity = false
        zone1.physicsBody?.categoryBitMask = zoneCat
        zone1.physicsBody?.contactTestBitMask = kanyeCat | ballCat
        zone1.physicsBody?.collisionBitMask = kanyeCat | ballCat
        addChild(zone1)
        
        ball.size = CGSize(width: 70, height: 70)
        ball.physicsBody = SKPhysicsBody (circleOfRadius: 25)
        ball.zPosition = 2
        ball.physicsBody?.isDynamic = false;
        ball.position = CGPoint(x: 455, y: 315)
        ball.physicsBody?.affectedByGravity = false
        ball.physicsBody?.categoryBitMask = ballCat
        ball.physicsBody?.contactTestBitMask = zoneCat | kanyeCat
        ball.physicsBody?.collisionBitMask = zoneCat | kanyeCat
        addChild(ball)
        
        let leftButton = UIButton(frame: CGRect(x: 25, y: 500, width: 70, height: 40))
        leftButton.backgroundColor = .gray
        leftButton.setTitle("Left", for: .normal)
        leftButton.addTarget(self, action: #selector(leftButtonAct), for: .touchUpInside)
        
        self.view?.addSubview(leftButton)
        
        let rightButton = UIButton(frame: CGRect(x: 220, y: 500, width: 70, height: 40))
        rightButton.backgroundColor = .gray
        rightButton.setTitle("Right", for: .normal)
        rightButton.addTarget(self, action: #selector(rightButtonAct), for: .touchUpInside)
        
        self.view?.addSubview(rightButton)
        
        //let moveToCenter = SKAction.move(to: CGPoint(x: self.frame.width/2,y: self.frame.height/2), duration:1.5)
        //kanye.run(moveToCenter)

    }
    

    func didBegin(_ contact: SKPhysicsContact) {
        print("did contact begin!")
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
        {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else
        {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if ((firstBody.categoryBitMask == kanyeCat) && (secondBody.categoryBitMask == ballCat)) {
            kanye.position = CGPoint(x: 300, y: 800)

        } else if ((firstBody.categoryBitMask == kanyeCat) && (secondBody.categoryBitMask == zoneCat)) {
            let endScene = EndScene(fileNamed:"EndScene")
            endScene?.scaleMode = .aspectFill
            self.view?.presentScene(endScene!, transition: SKTransition.fade(withDuration: 0.5))
        }
    }
    
    
    
    func leftButtonAct(sender: UIButton!) {
        let circle = UIBezierPath(ovalIn: CGRect(x:415, y: 320, width: 180, height: 170))
        let circularMove = SKAction.follow(circle.cgPath, asOffset: false, orientToPath: true, duration: 1.0)
        ball.run(SKAction.repeat(circularMove, count: 1))
        
    }
    func rightButtonAct(sender: UIButton!) {
        let circle = UIBezierPath(ovalIn: CGRect(x:425, y: 320, width: 180, height: 170))
        let circularMove = SKAction.follow(circle.cgPath, asOffset: false, orientToPath: true, duration: 1.0)
        ball.run(SKAction.repeat(circularMove, count: 1).reversed())
    }
   
    override func update(_ currentTime: TimeInterval) {
        if (kanye.position.x < 150 || kanye.position.x > 750) {
            kanye.position = CGPoint(x: 400, y: 700)
        }
        if (kanye.position.y < 0 || kanye.position.y > 800) {
            kanye.position = CGPoint(x: 400, y: 700)
        }
    }
}
