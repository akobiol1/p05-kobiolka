//
//  StartScene.swift
//  Dont let Kanye into his zone
//
//  Created by Amanda Kobiolka on 3/25/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

import Foundation
import SpriteKit

class StartScene: SKScene {
    let background = SKSpriteNode(imageNamed: "blackstars.jpg")

    override func didMove(to view: SKView) {
        /* Setup your scene here */
       /* background.anchorPoint = CGPoint.zero
        background.zPosition = 1
        addChild(background)*/
        let welcomeLabel = SKLabelNode(fontNamed: "Helvetica")
        let welcomeLabel2 = SKLabelNode(fontNamed: "Helvetica")
        
        welcomeLabel.text = "Don't let Kanye into his zone!"
        welcomeLabel.fontSize = 25
        welcomeLabel.position = CGPoint(x:self.frame.midX, y:self.frame.midY)
        self.addChild(welcomeLabel)
        
        welcomeLabel2.text = "Click anywhere to begin playing"
        welcomeLabel2.fontSize = 25
        welcomeLabel2.position = CGPoint(x:self.frame.midX, y:self.frame.midY-50)
        
        self.addChild(welcomeLabel2)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let playScene = GameScene(fileNamed:"GameScene")
        playScene?.scaleMode = .aspectFill
        self.view?.presentScene(playScene!, transition: SKTransition.fade(withDuration: 0.5))
    }
    
    
}
