//
//  EndScene.swift
//  Dont let Kanye into his zone
//
//  Created by Amanda Kobiolka on 3/25/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

import Foundation
import SpriteKit

class EndScene: SKScene {
    let background = SKSpriteNode(imageNamed: "blackstars.jpg")
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
       /* background.anchorPoint = CGPoint.zero
        background.zPosition = 1
        addChild(background)*/
        
        let endLabel = SKLabelNode(fontNamed: "Helvetica")
        let endLabel2 = SKLabelNode(fontNamed: "Helvetica")
        
        endLabel.text = "Oh no, game over :("
        endLabel.fontSize = 50
        endLabel.position = CGPoint(x:self.frame.midX, y:self.frame.midY)
        self.addChild(endLabel)
        
        endLabel2.text = "Click anywhere to play again"
        endLabel2.fontSize = 50
        endLabel2.position = CGPoint(x:self.frame.midX, y:self.frame.midY-100)
        
        self.addChild(endLabel2)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let playScene = GameScene(fileNamed:"GameScene")
        playScene?.scaleMode = .aspectFill
        self.view?.presentScene(playScene!, transition: SKTransition.fade(withDuration: 0.5))
    }
    
    
}
